import os

import cv2
import numpy as np
import tensorflow as tf
from autolab_core import YamlConfig
from mrcnn import model as modellib

from sd_maskrcnn.config import MaskConfig


class NetRunner:
    def __init__(self, conf_path='cfg/benchmark.yaml'):
        # Tensorflow configuration
        self._tf_config = tf.ConfigProto()
        self._tf_config.gpu_options.allow_growth = True

        # Model configuration
        self._config = YamlConfig(conf_path)
        image_shape = self._config['model']['settings']['image_shape']
        self._config['model']['settings']['image_min_dim'] = min(image_shape)
        self._config['model']['settings']['image_max_dim'] = max(image_shape)
        self._config['model']['settings']['gpu_count'] = 1
        self._config['model']['settings']['images_per_gpu'] = 1
        self._config['model']['settings']['image_channel_count'] = 3

        self._inference_config = MaskConfig(self._config['model']['settings'])

        # !!!!!!!!!!!!!!!!!!!!! # TODO
        self._sess = tf.Session(config=self._tf_config)
        self._sess.__enter__()
        self._init_model()
        # ????????????????????? #
        # ???????     ????????? #
        # ???????     ????????? #
        # ???????     ????????? #
        # ???????     ????????? #
        # ???????     ????????? #
        # ???????     ????????? #
        # ????????   ?????????? #
        # ????????????????????? #
        # ????????????????????? #
        # ???????     ????????? #
        # ???????     ????????? #
        # ????????????????????? #

    def _init_model(self):
        model_dir, _ = os.path.split(self._config['model']['path'])
        self.model = modellib.MaskRCNN(mode=self._config['model']['mode'], config=self._inference_config,
                                       model_dir=model_dir)
        print("Loading weights from ", self._config['model']['path'])
        self.model.load_weights(self._config['model']['path'], by_name=True)

    def _detect(self, images):
        self.model.config.BATCH_SIZE = len(images)
        results = self.model.detect(images, verbose=0)
        return results

    @staticmethod
    def _filter_segments(result, bin_mask, overlap_thresh=0.5):
        r = result
        deleted_masks = []
        num_detects = r['masks'].shape[2]
        # find segmsks to delete
        for k in range(num_detects):
            # compute the area of the overlap.
            inter = np.logical_and(bin_mask, r['masks'][:, :, k])
            frac_overlap = np.sum(inter) / np.sum(r['masks'][:, :, k])
            if frac_overlap <= overlap_thresh:
                deleted_masks.append(k)

        r['masks'] = [r['masks'][:, :, k] for k in range(num_detects) if k not in deleted_masks]
        r['masks'] = np.stack(r['masks'], axis=2) if r['masks'] else np.array([])
        r['rois'] = [r['rois'][k, :] for k in range(num_detects) if k not in deleted_masks]
        r['rois'] = np.stack(r['rois'], axis=0) if r['rois'] else np.array([])
        r['class_ids'] = np.array([r['class_ids'][k] for k in range(num_detects)
                                   if k not in deleted_masks])
        r['scores'] = np.array([r['scores'][k] for k in range(num_detects)
                                if k not in deleted_masks])
        return result

    def run(self, depth_images: list, bin_mask: np.ndarray = None) -> list:
        """
        :param depth_image: Depth images in grayscale - HxWx3 (512x512 by default) uint8 array
        :param bin_mask: Segmentation init mask (background==0, object==255) - HxW uint8 array
        :return: Result tuple: objects binary masks - HxWxN bool arrays
        """
        results = self._detect(depth_images)
        # filter detected segments with bin mask
        if bin_mask is not None:
            results = self._filter_segments(results, bin_mask, overlap_thresh=self._config['mask']['overlap_thresh'])
        # visualize.display_instances(depth_image, r['rois'], r['masks'], r['class_ids'], ['bg', 'obj'])

        masks = [r['masks'] for r in results]
        return masks

    def __del__(self):
        self._sess.__exit__(None, None, None)


def cvt_res_sdmask(r, gt):
    """
    Preprocessing of the estimated masks and ground truth
    """
    masks_r = r
    masks_gt = [np.dstack([g == val for val in np.unique(g)[1:]]) for g in gt]
    return masks_r, masks_gt


if __name__ == '__main__':
    from agd_bench.data_reader import *
    from agd_bench.evaluator import Evaluator
    from_file = False

    if from_file:
        depth_batch = [cv2.imread(f'../../wisdom-real/depth_ims/image_00000{n}.png') for n in (4, 5)]
        gt_batch = [cv2.imread(f'../../wisdom-real/modal_segmasks/image_00000{n}.png', 0) for n in (4, 5)]
    else:
        db = DataButcher(net_type=NetEnum.SD_MASK, basic_dir='../../unpacked/')
        # depth, _, gt = db.get_single_sd_mask(0)
        # depth_batch = [depth]
        # gt_batch = [gt]

    nr = NetRunner()
    ev = Evaluator()
    for depth_batch, gt_batch in db.data_generator():
        results_batch = nr.run(depth_batch, None)
        ev.update_batch(results_batch, gt_batch)
