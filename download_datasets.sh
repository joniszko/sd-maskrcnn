mkdir ./datasets ./datasets/wisdom

# WISDOM_REAL
wget https://berkeley.box.com/shared/static/7aurloy043f1py5nukxo9vop3yn7d7l3.rar -P ./datasets/wisdom
unrar x ./datasets/wisdom/7aurloy043f1py5nukxo9vop3yn7d7l3.rar ./datasets/wisdom
rm ./datasets/wisdom/7aurloy043f1py5nukxo9vop3yn7d7l3.rar

# WISDOM_SIM
wget  https://berkeley.box.com/shared/static/laboype774tjgu7dzma3tmcexhdd8l5a.rar -P ./datasets/wisdom
unrar x ./datasets/wisdom/laboype774tjgu7dzma3tmcexhdd8l5a.rar ./datasets/wisdom
rm ./datasets/wisdom/laboype774tjgu7dzma3tmcexhdd8l5a.rar
