from typing import Tuple
import tensorflow as tf
import os
import cv2
import numpy as np
from autolab_core import YamlConfig
from mrcnn import model as modellib, visualize
# from agd_bench.data_reader import *
from scipy.special import comb
from wheel.cli.pack import pack

from agd_bench.data_reader import DataButcher, SetEnum
from sd_maskrcnn.config import MaskConfig


class Model:
    def __init__(self, config, inference_config):
        model_dir, _ = os.path.split(config['model']['path'])
        self.model = modellib.MaskRCNN(mode=config['model']['mode'], config=inference_config,
                                       model_dir=model_dir)
        print("Loading weights from ", config['model']['path'])
        self.model.load_weights(config['model']['path'], by_name=True)

    def detect(self, image):
        results = self.model.detect([image], verbose=0)
        result = results[0]
        return result

    def detect_batch(self, images):
        results = self.model.detect(images, verbose=0)
        result = results[0]
        return result


def filter_segments(result, bin_mask, overlap_thresh=0.5):
    r = result
    deleted_masks = []
    num_detects = r['masks'].shape[2]
    # find segmsks to delete
    for k in range(num_detects):
        # compute the area of the overlap.
        inter = np.logical_and(bin_mask, r['masks'][:, :, k])
        frac_overlap = np.sum(inter) / np.sum(r['masks'][:, :, k])
        if frac_overlap <= overlap_thresh:
            deleted_masks.append(k)

    r['masks'] = [r['masks'][:, :, k] for k in range(num_detects) if k not in deleted_masks]
    r['masks'] = np.stack(r['masks'], axis=2) if r['masks'] else np.array([])
    r['rois'] = [r['rois'][k, :] for k in range(num_detects) if k not in deleted_masks]
    r['rois'] = np.stack(r['rois'], axis=0) if r['rois'] else np.array([])
    r['class_ids'] = np.array([r['class_ids'][k] for k in range(num_detects)
                               if k not in deleted_masks])
    r['scores'] = np.array([r['scores'][k] for k in range(num_detects)
                            if k not in deleted_masks])
    return result


def single_run(depth_image: np.ndarray, bin_mask: np.ndarray = None) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    :param depth_image: Depth image in grayscale - HxWx3 (512x512 by default) uint8 array
    :param bin_mask: Segmentation init mask (background==0, object==255) - HxW uint8 array
    :return: Result tuple: (
    ROIs of detected objects - Nx4 int32 array,
    class ids - 0,N int 32 array,
    objects binary masks - HxWxN bool array
    )
    """
    conf_path = 'cfg/benchmark.yaml'

    # === Prepare configuration
    tf_config = tf.ConfigProto()
    tf_config.gpu_options.allow_growth = True

    config = YamlConfig(conf_path)
    image_shape = config['model']['settings']['image_shape']
    config['model']['settings']['image_min_dim'] = min(image_shape)
    config['model']['settings']['image_max_dim'] = max(image_shape)
    config['model']['settings']['gpu_count'] = 1
    config['model']['settings']['images_per_gpu'] = 1
    config['model']['settings']['image_channel_count'] = 3

    inference_config = MaskConfig(config['model']['settings'])

    # === Run inference
    with tf.Session(config=tf_config) as sess:
        #
        # set_session(sess)
        # init model
        m = Model(config, inference_config)
        # detect segments
        r = m.detect(depth_image)
        # filter detected segments with bin mask
        if bin_mask is not None:
            r = filter_segments(r, bin_mask, overlap_thresh=config['mask']['overlap_thresh'])
        visualize.display_instances(depth_image, r['rois'], r['masks'], r['class_ids'], ['bg', 'obj'])

    return r['rois'], r['class_ids'], r['masks']


def cvt_res_sdmask(r, gt):
    """
    Preprocessing of the estimated masks and ground truth
    """
    masks_r = r
    masks_gt = np.dstack([gt == val for val in np.unique(gt)[1:]])
    return masks_r, masks_gt


if __name__ == '__main__':
    depth_iamge = cv2.imread('../dataset_samples/wisdom-real/high-res/depth_ims/image_000005.png')
    bin_mask = cv2.imread('../dataset_samples/wisdom-real/high-res/segmasks_filled/image_000005.png', 0)
    gt = cv2.imread('../dataset_samples/wisdom-real/high-res/modal_segmasks/image_000005.png', 0)
    ret = single_run(depth_iamge, None)
    from agd_bench.evaluator import Evaluator
    ev = Evaluator()
    ev.update(*cvt_res_sdmask(ret[2], gt))
    print('KUUUURWA')
    # db = DataButcher(SetEnum.ZED, basic_dir='../../unpacked')
    # single_run(**db.get_single_sd_mask(zed=True))
    # print('Finished')
    # single_run(depth_iamge, bin_mask)