import argparse
import tensorflow as tf
import os
import cv2
import numpy as np
from autolab_core import YamlConfig
from keras.backend.tensorflow_backend import set_session

from mrcnn import model as modellib, utils as utilslib, visualize
from sd_maskrcnn.config import MaskConfig


class Model:
    def __init__(self, config, inference_config):
        model_dir, _ = os.path.split(config['model']['path'])
        self.model = modellib.MaskRCNN(mode=config['model']['mode'], config=inference_config,
                                       model_dir=model_dir)
        print("Loading weights from ", config['model']['path'])
        self.model.load_weights(config['model']['path'], by_name=True)

    def detect(self, image):
        results = self.model.detect([image], verbose=0)
        result = results[0]
        return result


def load_image(image_path, config, grayscale=False):
    img = cv2.imread(image_path)
    if grayscale:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)[:,:,np.newaxis]
    img, _, _, _, _ = utilslib.resize_image(
        img,
        min_dim=config.IMAGE_MIN_DIM,
        min_scale=config.IMAGE_MIN_SCALE,
        max_dim=config.IMAGE_MAX_DIM,
        mode=config.IMAGE_RESIZE_MODE
    )
    if grayscale:
        img = img.squeeze()
    return img


def filter_segments(result, bin_mask, overlap_thresh=0.5):
    r = result
    deleted_masks = []
    num_detects = r['masks'].shape[2]
    # find segmsks to delete
    for k in range(num_detects):
        # compute the area of the overlap.
        inter = np.logical_and(bin_mask, r['masks'][:, :, k])
        frac_overlap = np.sum(inter) / np.sum(r['masks'][:, :, k])
        if frac_overlap <= overlap_thresh:
            deleted_masks.append(k)

    r['masks'] = [r['masks'][:, :, k] for k in range(num_detects) if k not in deleted_masks]
    r['masks'] = np.stack(r['masks'], axis=2) if r['masks'] else np.array([])
    r['rois'] = [r['rois'][k, :] for k in range(num_detects) if k not in deleted_masks]
    r['rois'] = np.stack(r['rois'], axis=0) if r['rois'] else np.array([])
    r['class_ids'] = np.array([r['class_ids'][k] for k in range(num_detects)
                               if k not in deleted_masks])
    r['scores'] = np.array([r['scores'][k] for k in range(num_detects)
                            if k not in deleted_masks])
    return result


def predict_mask(image_path, conf_path):
    # === Prepare configuration
    tf_config = tf.ConfigProto()
    tf_config.gpu_options.allow_growth = True

    config = YamlConfig(conf_path)
    image_shape = config['model']['settings']['image_shape']
    config['model']['settings']['image_min_dim'] = min(image_shape)
    config['model']['settings']['image_max_dim'] = max(image_shape)
    config['model']['settings']['gpu_count'] = 1
    config['model']['settings']['images_per_gpu'] = 1

    inference_config = MaskConfig(config['model']['settings'])

    img = None  # Depth image
    bin_mask = None  # Mask image

    if image_path.endswith('npy'):  # Numpy depth image
        depth_arr = np.load(image_path)
        if isinstance(depth_arr[0][0], np.floating):  # STIOS dataset
            depth_arr = np.nan_to_num(depth_arr)
            depth_arr[depth_arr < 0] = 0
            depth_arr[depth_arr == 0] = depth_arr[depth_arr != 0].min()
            filter_max = depth_arr.mean() + depth_arr.std()*4
            depth_arr[depth_arr > filter_max] = np.max(depth_arr[depth_arr <= filter_max])
            # Convert to cv2 img
            depth_img = cv2.normalize(depth_arr, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
            cv2.imshow('IMG_DEPTH', depth_img)
            closed = cv2.morphologyEx(depth_img, cv2.MORPH_CLOSE, np.ones((10, 10)), iterations=5)
            depth_img[depth_img == 0] = closed[depth_img == 0]

            # load segmask from output img (temp) -> ../gt/*.png
            mask_path = '/'.join(image_path.split('/')[:-2] + ['gt', f'{image_path[-6:-4]}.png'])
            mask_img = cv2.imread(mask_path, 0)
            _, mask_img = cv2.threshold(mask_img, 0, 255, cv2.THRESH_BINARY)

            # resize
            img = cv2.cvtColor(cv2.resize(depth_img, image_shape), cv2.COLOR_GRAY2BGR)
            bin_mask = cv2.resize(mask_img, image_shape)
            bin_mask = cv2.morphologyEx(bin_mask, cv2.MORPH_CLOSE, np.ones((5, 5)), iterations=1)
            pass
    cv2.imshow('IMG', img); cv2.imshow('BIN_MASK', bin_mask); cv2.waitKey(-1)

    # === Run inference
    with tf.Session(config=tf_config) as sess:
        set_session(sess)
        # init model
        m = Model(config, inference_config)
        # detect segments
        r = m.detect(img)
        # filter detected segments with bin mask
        if bin_mask is not None:
            r = filter_segments(r, bin_mask, overlap_thresh=config['mask']['overlap_thresh'])
        # visualize the result
        visualize.display_instances(img, r['rois'], r['masks'], r['class_ids'], ['bg', 'obj'])


if __name__ == '__main__':
    conf_parser = argparse.ArgumentParser(description="Benchmark SD Mask RCNN model")
    conf_parser.add_argument("--config", action="store", default="cfg/benchmark.yaml",
                             dest="conf_file", type=str, help="path to the configuration file")
    conf_parser.add_argument("--image", action="store",
                             default="../dataset_samples/STIOS_zed/office_carpet/depth/02.npy",
                             dest="image_path", type=str, help="path to the depth image")
    conf_args = conf_parser.parse_args()

    predict_mask(conf_args.image_path, conf_args.conf_file)
